package blockly;

import cronapi.*;
import cronapi.rest.security.CronappSecurity;
import java.util.concurrent.Callable;



@CronapiMetaData(type = "blockly")
@CronappSecurity
public class Testechamar {

public static final int TIMEOUT = 300;

/**
 *
 * testechamar
 *
 * @author Manoel Luis Souza De Oliveira Júnior
 * @since 11/10/2022 17:06:52
 *
 */
public static Var Executar() throws Exception {
 return new Callable<Var>() {

   public Var call() throws Exception {
    cronapi.io.Operations.startDownload(
    Var.valueOf(
    Var.valueOf("Imagem/Xcode.png").getObjectAsString() +
    Var.valueOf("Xcode.png").getObjectAsString()),
    Var.valueOf("Xcode.png"));
    return Var.VAR_NULL;
   }
 }.call();
}

/**
 *
 * @author Manoel Luis Souza De Oliveira Júnior
 * @since 11/10/2022 17:06:52
 *
 */
public static Var TesteImagem2() throws Exception {
 return new Callable<Var>() {

   public Var call() throws Exception {
    cronapi.io.Operations.startDownload(
    Var.valueOf(
    cronapi.io.Operations.fileAppDir().getObjectAsString() +
    cronapi.io.Operations.fileSeparator().getObjectAsString() +
    Var.valueOf("Imagem2").getObjectAsString() +
    cronapi.io.Operations.fileSeparator().getObjectAsString() +
    Var.valueOf("Peso.png").getObjectAsString()),
    Var.valueOf("Peso.png"));
    return Var.VAR_NULL;
   }
 }.call();
}

}

